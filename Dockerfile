# syntax=docker/dockerfile:1.4

####################################################################################################
## Build Packages

FROM node:18-alpine AS builder
WORKDIR /app

ARG TARGETPLATFORM

ENV NODE_OPTIONS=--max-old-space-size=8192

RUN <<EOF
  if [ "$TARGETPLATFORM" = 'linux/arm64' ]; then
  	apk --no-cache add python3 build-base
  	ln -sf /usr/bin/python3 /usr/bin/python
  fi
EOF

COPY package.json .
RUN corepack enable && corepack prepare

COPY pnpm-lock.yaml .
RUN pnpm fetch

COPY . .

## # Download the static build of Litestream directly into the path & make it executable.
# This is done in the builder and copied as the chmod doubles the size.
ADD https://github.com/benbjohnson/litestream/releases/download/v0.3.8/litestream-v0.3.8-linux-amd64-static.tar.gz /tmp/litestream.tar.gz
RUN tar -C /usr/local/bin -xzf /tmp/litestream.tar.gz


RUN <<EOF
	pnpm install --recursive --offline --frozen-lockfile
	npm_config_workspace_concurrency=1 pnpm run build
	pnpm --filter directus deploy --prod dist
	cd dist
	# Regenerate package.json file with essential fields only
	# (see https://github.com/directus/directus/issues/20338)
	node -e '
		const f = "package.json", {name, version, type, exports, bin} = require(`./${f}`), {packageManager} = require(`../${f}`);
		fs.writeFileSync(f, JSON.stringify({name, version, type, exports, bin, packageManager}, null, 2));
	'
	mkdir -p extensions
EOF

####################################################################################################
## Create Production Image

FROM node:18-alpine AS runtime

RUN npm install --global pm2@5
RUN apk add bash

USER node

WORKDIR /app

EXPOSE 8055

COPY --from=builder --chown=node:node /app/ecosystem.config.cjs .
COPY --from=builder --chown=node:node /app/dist .

# Copy executable & Litestream from builder.
COPY --from=builder /usr/local/bin/litestream /usr/local/bin/litestream

COPY deploy/run.sh .
COPY deploy/litestream.yml .

CMD ["./run.sh"]

#CMD : \
#	&& node cli.js bootstrap \
#	&& litestream replicate -config litestream.yml -exec "pm2-runtime start ecosystem.config.cjs" \
#	;
