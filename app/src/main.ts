/* eslint-disable no-console */

// Note: the import order is important. Vue has to come first. (Unhead will break if that's first)
import { createApp } from 'vue';

import { getVueComponentName } from '@/utils/get-vue-component-name';
import { createHead } from '@unhead/vue';
import { createPinia } from 'pinia';
import App from './app.vue';
import { registerComponents } from './components/register';
import { registerDirectives } from './directives/register';
import { loadExtensions, registerExtensions } from './extensions';
import { i18n } from './lang/';
import { router } from './router';
import './styles/main.scss';
import { registerViews } from './views/register';

init();

async function init() {
  const app = createApp(App);

  app.use(i18n);
  app.use(createPinia());
  app.use(createHead());

  app.config.errorHandler = (err, vm, info) => {
    const source = getVueComponentName(vm);
    console.warn(`[app-${source}-error] ${info}`);
    console.warn(err);
    return false;
  };

  registerDirectives(app);
  registerComponents(app);
  registerViews(app);

  await loadExtensions();
  registerExtensions(app);

  // Add router after loading of extensions to ensure all routes are registered
  app.use(router);

  app.mount('#app');

  // Prevent the browser from opening files that are dragged on the window
  window.addEventListener('dragover', (e) => e.preventDefault(), false);
  window.addEventListener('drop', (e) => e.preventDefault(), false);
}
