#!/bin/bash
set -e

# Restore the database if it does not already exist.
if [ -f data/database.sqlite ]; then
	echo "Database already exists, skipping restore"
else
	echo "No database found, restoring from replica if exists"
	litestream restore -config litestream.yml -v -if-replica-exists -o data/database.sqlite data/database.sqlite
fi

# Run litestream with your app as the subprocess.
exec litestream replicate -config litestream.yml -exec "pm2-runtime start ecosystem.config.cjs"
